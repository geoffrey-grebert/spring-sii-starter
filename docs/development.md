# Development

## Style guides

* [Markdown](http://www.cirosantilli.com/markdown-style-guide/)

## Git Practices

Pull requests cannot be accepted if they contain merge commits.

Always do "git pull --rebase" and "git rebase" vs "git pull" or "git merge".

Always create a new branch for each merge request to avoid intermingling different features or fixes on the same branch.
