# Contributing

When contributing to this repository, please first discuss the change you wish to make via [Issue](/../issues) before making a change.

Please note we have a code of conduct, please follow it in all your interactions with the project.

If you'd like to contribute, please fork the repository and use a feature branch.
Merge requests are warmly welcome.

Please read [/docs/development.md](/doc/development.md) for details.

## Open issue

Before raising an issue to this project, please read the [FAQ](/FAQ.md).
Then, please search in the [issue tracker](/../issues) for similar entries before submitting your own,
there's a good chance somebody else had the same issue or feature proposal.

Please submit bugs using the 'Bug' issue template provided on the issue tracker.
The text in the parenthesis is there to help you with what to include.
