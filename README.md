# SII Spring starter parent

Spring Boot starter for SII projects.

This starter includes:

| Module | Version |
| ------ | ------- |
| [Spring Boot](https://spring.io/projects/spring-boot) | [2.0.3](https://docs.spring.io/spring-boot/docs/2.0.3.RELEASE/reference/htmlsingle/) |
| [Spring Cloud](http://projects.spring.io/spring-cloud/) | [Finchley](http://cloud.spring.io/spring-cloud-static/Finchley.RELEASE/single/spring-cloud.html) | [JaCoCo](https://www.eclemma.org/jacoco/) | 0.8.1 |
| [OWASP Dependency Check](https://www.owasp.org/index.php/OWASP_Dependency_Check) | 3.3.0 |
| [SonarQube Scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Maven) | 3.4.1.1170 |

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests to us.

## FAQ

Please read [FAQ.md](/doc/faq.md) for details.

## License

This project is licensed under the [Apache License Version 2.0](/LICENCE)
